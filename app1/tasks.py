from __future__ import absolute_import, unicode_literals
from celery.utils.log import get_task_logger
from app1.helpers import send_email_celery
from proj.celery import app


logger = get_task_logger(__name__)


@app.task(name='send_email_task')
def send_email_task(email, text):
	logger.info('Email is beeing sent')
	return send_email_celery(email, text)


@app.task()
def add(x, y):
	return x + y