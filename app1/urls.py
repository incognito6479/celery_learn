from django.urls import path
from app1.views import SendMailView, send_mail


urlpatterns = [
	path('', SendMailView.as_view(), name='send_mail'),
	path('send/mail/ajax', send_mail, name='send_mail_ajax'),
]