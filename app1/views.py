from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import JsonResponse
from app1.tasks import send_email_task
import json


class SendMailView(TemplateView):
	template_name = 'index.html'


def send_mail(request):
	body = json.loads(request.body)
	send_email_task.delay(body['email'], body['text'])
	context = {
		'response': 1
	}
	return JsonResponse(context)